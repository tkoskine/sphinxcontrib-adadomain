===========================
THIS REPOSITORY IS OBSOLETE
===========================

Go to https://github.com/adacore/sphinxcontrib-adadomain for the maintained repository.

==========
Ada Domain
==========

:author: Tero Koskinen <tero.koskinen@iki.fi>

About
=====

This extension adds Ada domain support to Sphinx.

